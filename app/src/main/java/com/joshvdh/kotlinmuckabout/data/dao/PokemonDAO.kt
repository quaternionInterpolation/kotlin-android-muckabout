package com.joshvdh.kotlinmuckabout.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.joshvdh.kotlinmuckabout.data.models.PokemonData

/**
 *  Created by Josh van den Heever on 13/12/18.
 */
@Dao
interface PokemonDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPokemon(pokemon: PokemonData)

    @Query("SELECT * FROM pokemondata ORDER BY id LIMIT :limit OFFSET :offset")
    fun getPokemon(offset: Int, limit: Int): LiveData<List<PokemonData>>

    @Query("SELECT * FROM pokemondata WHERE id = :id LIMIT 1")
    fun getPokemon(id: Int): LiveData<PokemonData>?

    @Query("DROP TABLE pokemondata")
    fun deleteAllPokemon()

    @Query
    fun deletePokemon(id: Int)
}