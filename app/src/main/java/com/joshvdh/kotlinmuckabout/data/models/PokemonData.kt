package com.joshvdh.kotlinmuckabout.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 *  Created by Josh van den Heever on 13/12/18.
 */
@Entity
class PokemonData {
    @PrimaryKey
    var id: Long? = null
    var name: String? = null
    var frontImage: String? = null
    var backImage: String? = null

    companion object {
        fun fromJSON(json: String): PokemonData {

        }
    }
}