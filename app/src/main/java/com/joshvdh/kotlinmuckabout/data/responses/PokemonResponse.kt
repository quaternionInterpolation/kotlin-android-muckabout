package com.joshvdh.kotlinmuckabout.data.responses

/**
 *  Created by Josh van den Heever on 13/12/18.
 */
data class PokemonResponse(
    val id: Long,

)

data class PokemonSpritesResponse(
    val back_default: String?,
    val front_default: String?

    //There are others but won't bother listing them all here
)