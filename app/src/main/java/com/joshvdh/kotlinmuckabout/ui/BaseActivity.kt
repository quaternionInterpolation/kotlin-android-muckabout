package com.joshvdh.kotlinmuckabout.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/**
 *  Created by Josh van den Heever on 13/12/18.
 */
abstract class BaseActivity: AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //
    }
}