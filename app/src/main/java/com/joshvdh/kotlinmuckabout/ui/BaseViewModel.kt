package com.joshvdh.kotlinmuckabout.ui

import androidx.lifecycle.ViewModel

/**
 *  Created by Josh van den Heever on 13/12/18.
 */
abstract class BaseViewModel: ViewModel() {

}